package test.modules.home.databinding;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import com.facebook.drawee.view.SimpleDraweeView;

import test.modules.home.pojo.Row;

/*Data binding class for the Video List Item*/
public class CountryDetailItemViewBinding extends BaseObservable {
    public String mImageUrl;
    public String mTitle;
    public String mDescription;

    public CountryDetailItemViewBinding(Row row) {
        mImageUrl = row.getImageHref();
        mTitle = row.getTitle();
        mDescription = row.getDescription();
    }

    /* load the video thumbnail*/
    @BindingAdapter({"profileImage"})
    public static void loadImage(SimpleDraweeView view, String url) {
        view.setImageURI(url);
    }

    public CharSequence getTitle() {
        return mTitle;
    }

    public CharSequence getDescription() {
        if (mDescription == null) {
            return "No description available";
        }
        return mDescription;
    }
}