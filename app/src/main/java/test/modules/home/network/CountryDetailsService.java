package test.modules.home.network;

import retrofit2.Call;
import retrofit2.http.GET;
import test.modules.home.pojo.Results;

/**
 * Interface to provide the list api calls to implement via retroFit.
 **/
public interface CountryDetailsService {
    /*Get the country details from the network*/
    @GET(".")
    Call<Results> getCountryDetailsList();
}
