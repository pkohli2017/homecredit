package test.modules.home.network;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import test.modules.home.pojo.Results;

/**
 * Class to provide the implement for the Retrofit to make the api calls.
 */
public class CountryDetailsRepository {
    /*Base url for the country details API*/
    private static final String COUNTRY_DETAIL_LIST_URL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json/";

    private MutableLiveData<Results> mMutableResults;
    private CountryDetailsService mApi;
    private Retrofit mRetrofitInstance;

    public CountryDetailsRepository(Type type, Object typeAdapter) {
        mRetrofitInstance = new Retrofit.Builder()
                .baseUrl(COUNTRY_DETAIL_LIST_URL)
                .addConverterFactory(createGSONConverter(type, typeAdapter))
                .build();

        this.mApi = mRetrofitInstance.create(CountryDetailsService.class);
        mMutableResults = new MutableLiveData<>();
    }


    private static Converter.Factory createGSONConverter(Type type, Object typeAdapter) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(type, typeAdapter);
        Gson gson = gsonBuilder.create();
        return GsonConverterFactory.create(gson);
    }

    /*Get the country list from the network
     @return MutableLiveData<Results>*/
    public MutableLiveData<Results> getCountryDetailsList() {
        Call<Results> call = mApi.getCountryDetailsList();

        call.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                Results results = response.body();
                if (results != null) {
                    mMutableResults.setValue(results);
                }
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                mMutableResults.setValue(null);
            }
        });

        return mMutableResults;
    }
}
