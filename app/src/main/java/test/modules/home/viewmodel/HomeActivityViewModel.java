package test.modules.home.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.google.gson.reflect.TypeToken;

import test.modules.home.gsonbuilder.GetCountryDetailsListDeserializer;
import test.modules.home.network.CountryDetailsRepository;
import test.modules.home.pojo.Results;


public class HomeActivityViewModel extends ViewModel {
    protected CountryDetailsRepository mCountryDetailsListDataSource;
    /*Data which is observed by the Activity and all the related fragments*/
    private MutableLiveData<Results> mResults;

    public HomeActivityViewModel() {
        injectDependencies();
    }

    private void injectDependencies() {
        mCountryDetailsListDataSource = new CountryDetailsRepository(new TypeToken<Results>() {
        }.getType(), new GetCountryDetailsListDeserializer());
    }

    /*******METHODS FOR COUNTRY DETAILS LIST-- STARTS *********/

    /*Get the country details list results*/
    public MutableLiveData<Results> getCountryDetailsResults() {
        if (mResults == null) {
            if (mResults == null || mResults.getValue() == null) {
                mResults = loadResults();
            }
        }
        return mResults;
    }

    /*Get the updated country details list results*/
    public MutableLiveData<Results> getUpdatedResults() {
        mResults = loadResults();
        return mResults;
    }

    /*******METHODS FOR COUNTRY DETAILS LIST-- END *********/

    /*Load the results from the network*/
    private MutableLiveData<Results> loadResults() {
        return mCountryDetailsListDataSource.getCountryDetailsList();
    }
}
