package test.modules.home.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.mobile.home.credit.R;

import java.util.List;

import test.modules.home.adapters.CountryDetailsListAdapter;
import test.modules.home.pojo.Results;
import test.modules.home.pojo.Row;
import test.modules.home.viewmodel.HomeActivityViewModel;

/**
 * Fragment to details of the country
 **/
public class CountryDetailsListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String FRAGMENT_TAG = CountryDetailsListFragment.class.getSimpleName();
    private FragmentActivity mActivity;
    private RecyclerView mRecyclerView;
    private HomeActivityViewModel mViewModel;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Results mResults;

    /*Observers required*/
    /* This observer is required for the country details list.*/
    private Observer mGetCountryDetailsListObserver;

    public static CountryDetailsListFragment newInstance() {
        CountryDetailsListFragment fragment = new CountryDetailsListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mViewModel = ViewModelProviders.of(mActivity).get(HomeActivityViewModel.class);
    }

    private void populateCountryDetailsList() {
        // Observe for the changes in the results
        mGetCountryDetailsListObserver = new Observer<Results>() {
            @Override
            public void onChanged(@Nullable Results results) {
                if (results != null) {
                    // the results are downloaded now. Update the list.
                    mResults = results;
                    setData(mResults.getRows());
                }
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        };

        mViewModel.getCountryDetailsResults().observe(mActivity, mGetCountryDetailsListObserver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_country_detail_list, container, false);
        initViews(view);
        populateCountryDetailsList();
        return view;
    }

    private void initViews(View view) {
        // set swipe refresh layout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        // set recycler view
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void setData(List<Row> rows) {
        CountryDetailsListAdapter adapter = (CountryDetailsListAdapter) mRecyclerView.getAdapter();
        if (adapter == null) {
            adapter = new CountryDetailsListAdapter(rows);
            mRecyclerView.setAdapter(adapter);
        } else {
            adapter.replaceData(rows);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // We don't want to observe any data change when this fragment is in the
        // current method and will be destroyed
        mViewModel.getCountryDetailsResults().removeObserver(mGetCountryDetailsListObserver);

    }

    @Override
    public void onRefresh() {
        // observe for any update on the server
        mViewModel.getUpdatedResults().observe(mActivity, mGetCountryDetailsListObserver);
    }
}
