package test.modules.home.gsonbuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import test.modules.home.pojo.Results;
import test.modules.home.pojo.Row;

/*Parse the data for the details of the country list API*/
public class GetCountryDetailsListDeserializer implements JsonDeserializer<Results> {

    @Override
    public Results deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Results results = new Results();
        final JsonObject jsonObject = json.getAsJsonObject();
        final String about = jsonObject.get("title").getAsString();
        results.setTitle(about);
        final JsonArray itemsJsonArray = jsonObject.getAsJsonArray("rows");
        if (itemsJsonArray == null || itemsJsonArray.size() == 0) {
            return results;
        }
        List<Row> rows = null;
        if (itemsJsonArray.size() > 0) {
            rows = new ArrayList<>();
            results.setRows(rows);
        }

        for (JsonElement itemsJsonElement : itemsJsonArray) {
            final JsonObject itemJsonObject = itemsJsonElement.getAsJsonObject();
            Row row = new Row();
            final JsonElement titleObject = itemJsonObject.get("title");
            if (titleObject != null && !titleObject.isJsonNull()) {
                final String title = titleObject.getAsString();
                row.setTitle(title);
            }

            final JsonElement descriptionObject = itemJsonObject.get("description");
            if (descriptionObject != null && !descriptionObject.isJsonNull()) {
                final String description = descriptionObject.getAsString();
                row.setDescription(description);
            }


            final JsonElement imageObject = itemJsonObject.get("imageHref");
            if (imageObject != null && !imageObject.isJsonNull()) {
                final String image = imageObject.getAsString();
                String trimmedUrl = image.substring(5, image.length());
                row.setImageHref("https:" + trimmedUrl);
            }
            // add the resource object in the list
            if (!titleObject.isJsonNull() || !descriptionObject.isJsonNull() || !imageObject.isJsonNull())
                rows.add(row);
        }
        return results;
    }
}
