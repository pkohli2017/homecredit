package test.modules.home.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mobile.home.credit.BR;
import com.android.mobile.home.credit.R;
import com.android.mobile.home.credit.databinding.LayoutCountryDetailItemBinding;

import java.util.List;

import test.modules.home.databinding.CountryDetailItemViewBinding;
import test.modules.home.pojo.Row;

/**
 * Adapter class to bind the data to the Recycler view.
 **/
public class CountryDetailsListAdapter extends RecyclerView.Adapter<CountryDetailsListAdapter.ViewHolder> {
    private List<Row> mCountryDetailsList;

    public CountryDetailsListAdapter(List<Row> list) {
        mCountryDetailsList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutCountryDetailItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.layout_country_detail_item, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Row result = mCountryDetailsList.get(position);
        viewHolder.bind(new CountryDetailItemViewBinding(result));
    }

    /*Replace the data on the list*/
    public void replaceData(List<Row> row) {
        mCountryDetailsList.clear();
        mCountryDetailsList.addAll(row);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mCountryDetailsList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        LayoutCountryDetailItemBinding binding;

        ViewHolder(LayoutCountryDetailItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(CountryDetailItemViewBinding model) {
            binding.setVariable(BR.viewmodel, model);
            binding.executePendingBindings();
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }
    }
}
