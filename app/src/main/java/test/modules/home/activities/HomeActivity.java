package test.modules.home.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.mobile.home.credit.R;
import com.facebook.drawee.backends.pipeline.Fresco;

import test.activities.BaseActivity;
import test.modules.home.fragments.CountryDetailsListFragment;
import test.modules.home.pojo.Results;
import test.modules.home.viewmodel.HomeActivityViewModel;

/**
 * Activity class to show country details list
 **/
public class HomeActivity
        extends BaseActivity {
    /*View model for the activity and related fragments to observe the data*/
    private HomeActivityViewModel mViewModel;
    /*Observers required for the country details list.*/
    private Observer mGetCountryDetailsListObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActionBar();
        //Initialize fresco, we will downloading the images soon for the list.
        Fresco.initialize(this);
        setContentView(R.layout.activity_home, null);
        mViewModel = ViewModelProviders.of(this).get(HomeActivityViewModel.class);
        showCountryDetailsListFragment();
        fetchCountryDetailsList();
    }

    private void fetchCountryDetailsList() {
        // fetch the country list from the  saved state or network
        // show progress dialog before fetching the results.
        showProgressDialog();
        mGetCountryDetailsListObserver = new Observer<Results>() {
            @Override
            public void onChanged(@Nullable Results results) {
                if (results == null) {
                    // show the message that the request is failure.
                    Toast.makeText(HomeActivity.this, R.string.network_error, Toast.LENGTH_SHORT).show();
                } else {
                    // Now update the action bar title.
                    updateActionBarTitle(results.getTitle());
                }
                // hide progress dialog
                hideProgressDialog();
            }
        };
        mViewModel.getCountryDetailsResults().observe(this, mGetCountryDetailsListObserver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // We don't want to observe any data change when this fragment is in the
        // current method and will be destroyed
        mViewModel.getCountryDetailsResults().removeObserver(mGetCountryDetailsListObserver);
    }

    private void initActionBar() {
        // Initialize action bar Set No title. This will be updated
        // later when we have results ready
        initActionBar(BaseActivity.INVALID_TITLE, BaseActivity.INVALID_HOME_ICON, true);
        showActionBar();
    }

    /*Show progress dialog*/
    @Override
    public void showProgressDialog() {
        super.showProgressDialog();
    }

    private void showCountryDetailsListFragment() {
        CountryDetailsListFragment countryDetailsListFragment = CountryDetailsListFragment.newInstance();
        showFragment(R.id.main_content_view, countryDetailsListFragment, CountryDetailsListFragment.FRAGMENT_TAG, false, true);
    }
}
