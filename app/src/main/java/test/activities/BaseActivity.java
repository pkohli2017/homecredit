package test.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.mobile.home.credit.R;

public class BaseActivity extends AppCompatActivity {
    public static int INVALID_HOME_ICON = -1;
    public static int INVALID_TITLE = -1;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    /*Set the content View*/
    protected void setContentView(int layoutResID, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = vi.inflate(layoutResID, parent);
        FrameLayout layout = (FrameLayout) findViewById(R.id.content_view);
        layout.addView(contentView);
    }

    /* Initialize the action bar*/
    protected void initActionBar(int titleResId, int homeIcon, boolean displayHomeIcon) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            Drawable bgDrawable = getResources().getDrawable(R.drawable.actionbar_background);
            actionBar.setBackgroundDrawable(bgDrawable);
            if (titleResId == INVALID_TITLE) {
                actionBar.setTitle(R.string.empty_title);
            } else {
                actionBar.setTitle(titleResId);
            }

            if (displayHomeIcon && homeIcon != INVALID_HOME_ICON) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeAsUpIndicator(homeIcon);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    /* Update the title on action bar*/
    protected void updateActionBarTitle(String titleResId) {
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(titleResId);
    }

    /* Show fragment*/
    protected void showFragment(int fragmentContainerId, Fragment fragment, String fragmentTag,
                                boolean addToBackStack, boolean withAnimation) {
        showFragment(fragmentContainerId, fragment, fragmentTag, addToBackStack, withAnimation,
                R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_left_in, R.anim.slide_out_right);
    }

    private void showFragment(
            int fragmentContainerId, Fragment fragment, String fragmentTag, boolean addToBackStack,
            boolean withAnimation, int animEnter, int animExit, int animPopEnter, int animPopExit) {
        Fragment previous = getSupportFragmentManager().findFragmentByTag(fragmentTag);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        if (previous != null) {
            ft = previous.getActivity().getSupportFragmentManager().beginTransaction();
        }
        if (withAnimation) {
            ft.setCustomAnimations(animEnter, animExit, animPopEnter, animPopExit);
        }
        ft.replace(fragmentContainerId, fragment, fragmentTag);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }

    /* Show Progress Dialog*/
    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.show();
    }

    /* Hide Progress Dialog*/
    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    /* Show Action bar*/
    protected void showActionBar() {
        getSupportActionBar().show();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (count >= 1) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
    }
}

